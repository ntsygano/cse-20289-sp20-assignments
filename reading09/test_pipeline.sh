#!/bin/bash

SCRIPT=${1:-pipeline.py}
WORKSPACE=/tmp/$SCRIPT.$(id -u)
FAILURES=0

export LC_ALL=C

error() {
    echo "$@"
    echo
    [ -r $WORKSPACE/test ] && cat $WORKSPACE/test
    echo
    FAILURES=$((FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

output() {
    cat /etc/passwd | cut -d : -f 7 | sort | uniq -c | sort -rns
}

mkdir $WORKSPACE

trap "cleanup" EXIT
trap "cleanup 1" INT TERM

echo "Testing $SCRIPT ..."

printf " %-40s ... " "$SCRIPT"
diff -y <(./$SCRIPT) <(output) &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "no system, popen, subprocess"
grep -E '(system|popen|subprocess)' $SCRIPT &> $WORKSPACE/test
if [ $? -eq 0 ]; then
    error "Failure"
else
    echo "Success"
fi

TESTS=$(($(grep -c Success $0) - 1))
echo "   Score $(echo "scale=2; ($TESTS - $FAILURES) / $TESTS.0 * 1.0" | bc | awk '{printf "%0.2f\n", $1}')"
echo
