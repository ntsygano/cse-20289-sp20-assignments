#!/usr/bin/env python3

import collections
import re
import requests

# Constants

URL = 'https://cse.nd.edu/people/faculty'

# Initialize a dictionary with integer values
counts = collections.defaultdict(int)

# TODO: Make a HTTP request to URL
response = None

# TODO: Access text from response object
data = None

# TODO: Compile regular expression to extract degrees and years of each faculty
# member
regex = None

# TODO: Search through data using compiled regular expression and count up all
# the faculty members per year.
pass

# TODO: Sort items in counts by key in reverse order
items = {}

# Sort items by value in reverse order and display counts and years
for year, count in sorted(items, key=lambda p: p[1], reverse=True):
    print(f'{count:>7} {year}')
