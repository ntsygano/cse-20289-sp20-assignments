#!/usr/bin/env python3

import socket
import sys

# Parse command line options
try:
    HOST = sys.argv[1]
    PORT = int(sys.argv[2])
except IndexError:
    print("Usage: {} HOST PORT".format(sys.argv[0]), file=sys.stderr)
    sys.exit(1)

# Create socket and connect to specified HOST and PORT
try:
    csocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    csocket.connect((HOST, PORT))
    cstream = csocket.makefile('w')
except socket.error as e:
    print('Socket Error: {}'.format(e))
    sys.exit(1)

# Read from stdin and write to socket
for line in sys.stdin:
    cstream.write(line)

# Cleanup
csocket.close()
