#!/usr/bin/env python3

import atexit
import os
import sys
import tempfile

# Functions

def usage(status=0):
    ''' Display usage message for program and exit with specified status. '''
    print(f'Usage: {os.path.basename(sys.argv[0])} files...')
    sys.exit(status)

def save_files(files):
    ''' Save list of files to a temporary file and return the name of the
    temporary file. '''
    pass

def edit_files(path):
    ''' Fork a child process to edit the file specified by path using the user's
    default EDITOR (use "vim" if not set).  Parent waits for child process and
    returns whether or not the child was successful. '''
    return False

def move_files(files, path):
    ''' For each file in files and the corresponding information from the file
    specified by path, rename the original source file to the new target path
    (if the names are different).  Return whether or not all files were
    successfully renamed. '''
    return False

def main():
    ''' Parse command line arguments, save arguments to temporary file, allow
    the user to edit the temporary file, move the files, and remove the
    temporary file. '''
    # TODO: Parse command line arguments
    pass

    # TODO: Save files (arguments)
    pass

    # TODO: Register unlink to cleanup temporary file
    pass

    # TODO: Edit files stored in temporary file
    pass

    # TODO: Move files stored in temporary file
    pass

# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
