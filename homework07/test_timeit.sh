#!/bin/bash

SCRIPT=timeit.py
WORKSPACE=/tmp/$SCRIPT.$(id -u)
FAILURES=0

export PATH=$WORKSPACE:$PATH

# Functions

error() {
    echo "$@"
    [ -r $WORKSPACE/test ] && (echo; cat $WORKSPACE/test; ls -l $WORKSPACE; echo)
    FAILURES=$((FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

test_output() {
    ttime="$(awk '/Time Elapsed:/ {print $3}' $WORKSPACE/test)"
    if ! echo "$ttime" | grep -q -P "$1"; then
    	echo "Wrong time: $ttime != $1" >> $WORKSPACE/test
	error "Failure"
    else
	echo "Success"
    fi
}

grep_all() {
    for pattern in $1; do
    	if ! grep -i -q -E "$pattern" $2; then
    	    echo "Missing $pattern in $2" >> $WORKSPACE/test
    	    return 1;
    	fi
    done
    return 0;
}

SYSCALLS="fork exec[vl]p (wait|waitpid) signal time.time WIFEXITED WEXITSTATUS WTERMSIG"

# Setup

mkdir $WORKSPACE

trap "cleanup" EXIT
trap "cleanup 1" INT TERM

# Testing

echo "Testing $SCRIPT..."

printf " %-72s ... " "system calls"
if ! grep_all "$SYSCALLS" $SCRIPT; then
    error "Failure"
else
    echo "Success"
fi

printf " %-72s ... " "usage (-h)"
PATTERNS="usage"
./$SCRIPT -h &> $WORKSPACE/test
if [ $? -ne 0 ] || ! grep_all "$PATTERNS" $WORKSPACE/test; then
    error "Failure"
else
    echo "Success"
fi

printf " %-72s ... " "usage (no arguments)"
PATTERNS="usage"
./$SCRIPT &> $WORKSPACE/test
if [ $? -eq 0 ] || ! grep_all "$PATTERNS" $WORKSPACE/test; then
    error "Failure"
else
    echo "Success"
fi

printf " %-72s ... " "usage (-t 5, no command)"
PATTERNS="usage"
./$SCRIPT -t 5 &> $WORKSPACE/test
if [ $? -eq 0 ] || ! grep_all "$PATTERNS" $WORKSPACE/test; then
    error "Failure"
else
    echo "Success"
fi

printf " %-72s ... " "sleep"
./$SCRIPT sleep &> $WORKSPACE/test
if [ $? -ne 1 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-72s ... " "sleep 1"
PATTERNS="Elapsed"
./$SCRIPT sleep 1 &> $WORKSPACE/test
if [ $? -ne 0 ] || ! grep_all "$PATTERNS" $WORKSPACE/test; then
    error "Failure"
else
    echo "Success"
fi
printf " %-72s ... " "sleep 1 (output)"   && test_output 1.\\d

printf " %-72s ... " "false"
./$SCRIPT false &> $WORKSPACE/test
if [ $? -ne 1 ]; then
    error "Failure"
else
    echo "Success"
fi
printf " %-72s ... " "false (output)"   && test_output 0.\\d

printf " %-72s ... " "date"
./$SCRIPT date &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi
printf " %-72s ... " "date (output)"   && test_output 0.\\d

printf " %-72s ... " "sleep 5"
./$SCRIPT sleep 5 &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi
printf " %-72s ... " "sleep 5 (output)"   && test_output 5.\\d

printf " %-72s ... " "-t 1 sleep 5"
PATTERNS="Elapsed"
./$SCRIPT -t 1 sleep 5 &> $WORKSPACE/test
if [ $? -ne 9 ] || ! grep_all "$PATTERNS" $WORKSPACE/test; then
    error "Failure"
else
    echo "Success"
fi
printf " %-72s ... " "-t 1 sleep 5 (output)"   && test_output 1.\\d

printf " %-72s ... " "-t 5 sleep 1"
PATTERNS="Elapsed"
./$SCRIPT -t 5 sleep 1 &> $WORKSPACE/test
if [ $? -ne 0 ] || ! grep_all "$PATTERNS" $WORKSPACE/test; then
    error "Failure"
else
    echo "Success"
fi
printf " %-72s ... " "-t 5 sleep 1 (output)"   && test_output 1.\\d

printf " %-72s ... " "-t 2 sleep 4"
PATTERNS="Elapsed"
./$SCRIPT -t 2 sleep 4 &> $WORKSPACE/test
if [ $? -ne 9 ] || ! grep_all "$PATTERNS" $WORKSPACE/test; then
    error "Failure"
else
    echo "Success"
fi
printf " %-72s ... " "-t 2 sleep 4 (output)"   && test_output 2.\\d

printf " %-72s ... " "-t 4 sleep 2"
PATTERNS="Elapsed"
./$SCRIPT -t 4 sleep 2 &> $WORKSPACE/test
if [ $? -ne 0 ] || ! grep_all "$PATTERNS" $WORKSPACE/test; then
    error "Failure"
else
    echo "Success"
fi
printf " %-72s ... " "-t 4 sleep 2 (output)"   && test_output 2.\\d

TESTS=$(($(grep -c Success $0) - 1))
echo "   Score $(echo "scale=2; ($TESTS - $FAILURES) / $TESTS.0 * 5.00" | bc | awk '{printf "%0.2f\n", $0}')"
echo
